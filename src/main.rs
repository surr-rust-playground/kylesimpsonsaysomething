extern crate reqwest;
use std::io::Read;
#[macro_use] extern crate lazy_static;
extern crate regex;
use regex::Regex;

fn main() {
    let html = get_random_wiki_article().unwrap();
    let titles = find_articles_links_titles(html);
    if titles.len() > 1 {
        println!("{}", compose_the_phrase_with(&titles[0], &titles[1]));
    }
}

fn find_articles_links_titles(html: String) -> Vec<String> {
    let mut titles: Vec<String> = Vec::new();
    lazy_static! {
        static ref RE: Regex = Regex::new(r####"class="mw-redirect"[^>]+>([a-zA-Z -]{3,})<"####).unwrap();
    }
    for cap in RE.captures_iter(&html) {
        titles.push(String::from(&cap[1]));
    }
    titles
}

fn get_random_wiki_article() -> Result<String, Box<std::error::Error>> {
    let mut res = reqwest::get("https://en.wikipedia.org/wiki/Special:Random")?;
    let mut body = String::new();
    res.read_to_string(&mut body)?;
    Ok(body)
}

fn compose_the_phrase_with(preferred: &str, last_option: &str) -> String {
    format!(
        "usage of '{}' should be preferred and '{}' should only be a last option",
        preferred, last_option
    )
}
