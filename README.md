The Kyle Simpson [said once](https://medium.com/@amsterdamjs/kyle-simpson-ive-forgotten-more-javascript-than-most-people-ever-learn-3bddc6c13e93) — **"usage of == should be preferred and === should only be a last option"**. And that's great!

This little ugly tool makes a request to [wikipedia's random article](https://en.wikipedia.org/wiki/Special:Random) and outputs the Kyle's great phrase with some random changes.
